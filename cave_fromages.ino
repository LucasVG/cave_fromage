#include "Wire.h"
#include "LiquidCrystal_I2C.h"
#include <OneWire.h>
#include <DS18B20.h>
#include <amt1001_ino.h>
#include <ezButton.h>

// pins
const int TEMP_SENSOR = 2; // Arduino pin connected to DS18B20 sensor's DQ pin
const int fridge = 3;
const int HUMID_SENSOR = A3;
const int humidif = 5;
const int fan = 6;
ezButton temp_button(7);
ezButton humid_button(8);
ezButton minus_button(9);
ezButton plus_button(10);

// output pins states
int fridge_state = LOW;
int humidif_state = LOW;
int fan_state = LOW;

// buttons timing
int holdTime = 500;
int longHoldTime = 2000;
unsigned long timePressed;

// states
enum states{noSet, setTargetTemp, setHystTemp, setTargetHumid, setHystHumid, setCooldown};
states currentState = noSet;

// LCD screen
LiquidCrystal_I2C LCD(0x27,16,2); // définit le type d'écran lcd 16 x 2

// temperature sensor
OneWire oneWire(TEMP_SENSOR);         // setup a oneWire instance 
DS18B20 tempSensor(&oneWire); // pass oneWire to DS18B20 library

// temperature and humidity variables and print
float temp;
char temp_print[] = "19.2";
float target_temp = 27;
char target_temp_print[] = "27.0";
float hyst_temp = 1;
char hyst_temp_print[] = "1.0";
float humid;
char humid_print[] = "90";
float dht_t;
char dht_t_print[] = "18.4";
float target_humid = 90;
char target_humid_print[] = "90";
float hyst_humid = 5;
char hyst_humid_print[] = "5";
float cooldown = 18;
char cooldown_print[] = "180";

unsigned long cooldownMillis = 0;

// time values for blinking text
unsigned long previousMillis = 0;
unsigned long currentMillis = 0;
const long blinkInterval = 500;

void setup() {
  // start serial port
  Serial.begin(9600);
  // initialisation de l'afficheur
   LCD.init();
   LCD.backlight();
   print_all();
   // initialisation du capteur de température
   tempSensor.begin();
   // initialisation des pin des boutons
   temp_button.setDebounceTime(40);
   humid_button.setDebounceTime(40);
   minus_button.setDebounceTime(40);
   plus_button.setDebounceTime(40);
   // initialisation des pin des régulateurs
   pinMode(fridge, OUTPUT);
   pinMode(humidif, OUTPUT);
   pinMode(fan, OUTPUT);
}

void print_blink_text(char* string, long previousTime) {
  currentMillis = millis();
  if (currentMillis - previousTime < 0.5*blinkInterval) {
    LCD.print("    ");
  }
  else if (currentMillis - previousTime < (blinkInterval * 2)) {
    LCD.print(string);
  }
  else {
    previousMillis = currentMillis;
  }   
}

void print_all() {
  dtostrf(temp, 4, 1, temp_print);
  dtostrf(target_temp, 4, 1, target_temp_print);
  dtostrf(hyst_temp, 3, 1, hyst_temp_print);
  dtostrf(humid, 2, 0, humid_print);
  dtostrf(target_humid, 2, 0, target_humid_print);
  dtostrf(hyst_humid, 1, 0, hyst_humid_print);
  dtostrf(cooldown*10, 3, 0, cooldown_print);

   LCD.setCursor(0,0);
   LCD.print("T");
   LCD.setCursor(2,0);
   LCD.print(temp_print);
   LCD.setCursor(7,0);
   if (currentState == setTargetTemp) {
    print_blink_text(target_temp_print, previousMillis);
   }
   else {
    LCD.print(target_temp_print);
   }
   LCD.setCursor(12,0);
   if (currentState == setHystTemp) {
    print_blink_text(hyst_temp_print, previousMillis);
   }
   else {
    LCD.print(hyst_temp_print);
   }
   LCD.setCursor(0,1);
   LCD.print("H");
   LCD.setCursor(2,1);
   LCD.print(humid_print);
   LCD.setCursor(5,1);
   if (currentState == setTargetHumid) {
    print_blink_text(target_humid_print, previousMillis);
   }
   else {
     LCD.print(target_humid_print);
   }
   LCD.setCursor(8,1);
   if (currentState == setHystHumid) {
    print_blink_text(hyst_humid_print, previousMillis);
   }
   else {
    LCD.print(hyst_humid_print);    
   }
   LCD.setCursor(10,1);
   LCD.print("CD ");
   if (currentState == setCooldown) {
    print_blink_text(cooldown_print, previousMillis);
   }
   else {
    LCD.print(cooldown_print);
   }
}

void loop() {
  // buttons loops
  temp_button.loop();
  humid_button.loop();
  minus_button.loop();
  plus_button.loop();
  
  // update temperature
  tempSensor.requestTemperatures();
//  while (!tempSensor.isConversionComplete());  // wait until sensor is ready
  temp = tempSensor.getTempC();
  
  // update humidity
  uint16_t step = analogRead(HUMID_SENSOR);
  double humid_volt = (double)step * (5.0 / 1023.0);
  uint16_t humid_uint = amt1001_gethumidity(humid_volt);
  humid = (float) humid_uint;
  
  // regulate temperature
  currentMillis = millis();
  if (temp > target_temp + hyst_temp && fridge_state == LOW && currentMillis - cooldownMillis > cooldown*10000) {
    fridge_state = HIGH;
    digitalWrite(fridge, fridge_state);
    fan_state = HIGH;
    digitalWrite(fan, fan_state);
  }
  else if (temp < target_temp - hyst_temp && fridge_state == HIGH) {
    fridge_state = LOW;
    digitalWrite(fridge, fridge_state);
    cooldownMillis = currentMillis;
    fan_state = LOW;
    digitalWrite(fan, fan_state);
  }

  // regulate humidity
  if (humid < target_humid - hyst_humid && humidif_state == LOW) {
    humidif_state = HIGH;
    digitalWrite(humidif, humidif_state);
  }
  else if (humid > target_humid + hyst_humid && humidif_state == HIGH) {
    humidif_state = LOW;
    digitalWrite(humidif, humidif_state);
  }
  
  // check buttons
  checkButtons();
  
  // print all
  print_all();
}

void checkButtons()
{
//  Serial.print("state :");
//  Serial.println(currentState);
//  int btnState = temp_button.getState();
//  Serial.print("etat bouton : ");
//  Serial.println(btnState);
  bool temp_pressed = false;
  bool humid_pressed = false;
  temp_pressed = temp_button.isPressed();
  humid_pressed = humid_button.isPressed();
  if (temp_pressed) {
    temp_pressed = true;
  }
  if (humid_pressed) {
    humid_pressed = true;
  }
  switch (currentState) {
  case noSet:
    if (temp_pressed) {
      currentState = setTargetTemp;
    }
    else if (humid_pressed) {
      currentState = setTargetHumid;
    }
    break;
  case setTargetTemp:
    if (temp_pressed) {
      currentState = setHystTemp;
    }
    else if (humid_pressed) {
      currentState = setTargetHumid;
    }
    else
      target_temp = updateVar(target_temp);
    break;
  case setHystTemp:
    if (temp_pressed) {
      currentState = setCooldown;
    }
    else if (humid_pressed) {
      currentState = setTargetHumid;
    }
    else
      hyst_temp = updateVar(hyst_temp);
    break;
  case setCooldown:
    if (temp_pressed) {
      currentState = noSet;
    }
    else if (humid_pressed) {
      currentState = setTargetHumid;
    }
    else {
      cooldown = updateVar(cooldown);
    }
    break;
  case setTargetHumid:
    if (temp_pressed) {
      currentState = setTargetTemp;
    }
    else if (humid_pressed) {
      currentState = setHystHumid;
    }
    else
      target_humid = updateVar(target_humid);
    break;
  case setHystHumid:
    if (temp_pressed) {
      currentState = setTargetTemp;
    }
    else if (humid_pressed) {
      currentState = noSet;
    }
    else {
     hyst_humid = updateVar(hyst_humid); 
    }
    break;
  }
}

float updateVar(float var) {
  if (plus_button.isPressed()) {
    timePressed = millis();
    var += 0.1;
  }
  else if (plus_button.getState() == LOW) {
    var = incrementVar(var, timePressed);
      }
  else if (minus_button.isPressed()) {
    timePressed = millis();
    var -= 0.1;
  }
  else if (minus_button.getState() == LOW) {
    var = decrementVar(var, timePressed);
  }
  return var;
}

float incrementVar(float var, unsigned long timePressed) {
  unsigned long currentTime = millis();
  if ((currentTime - timePressed) > longHoldTime) {
    var += 0.2;
    delay(100);
  } 
  else if ((currentTime - timePressed) > holdTime) {
    var += 0.05;
    delay(100);
  }
  return var;
}

float decrementVar(float var, unsigned long timePressed) {
  unsigned long currentTime = millis();
  if ((currentTime - timePressed) > longHoldTime) {
    var -= 0.2;
    delay(100);
  }
  else if ((currentTime - timePressed) > holdTime) {
    var -= 0.05;
    delay(100);
  }
  return var;
}
